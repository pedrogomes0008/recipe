# Recipe Search

This project is a recipe search made whit React. This application is connected to an API call EDAMAM, who gives a list of plates that we can look for.

## Initialize the app

First things first. To use this app, download as a zip file and then open in your IDE. This app was made using Visual Studio Code. Besides IDE, you need node.js in your pc as well npm or yarn.

https://code.visualstudio.com/ -> Link for IDE vsCode

https://nodejs.org/en/         -> Link for node (v14.17.1)


## Available Scripts

In the project directory, you can run:

### `npm i`

To install all the dependecies just put this command on the terminal in the root path of the downloaded file and will create the node_modules.

Another thing to use this project you need to create a .env.local file in the root of your project and put the environment variables in the that file. To have the value of the variables just create an account in https://developer.edamam.com/edamam-recipe-api and you will have your own variables.

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

# Final Result

![Alt-Text](src/images/Recipe.PNG)


