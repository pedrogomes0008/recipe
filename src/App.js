import React, { useEffect, useState } from 'react';
import Recipe from './component/Recipe';
import './App.css';

require('dotenv').config();

function App() {
	// const APP_ID = process.env.REACT_APP_APP_ID;
	// const APP_KEY = process.env.REACT_APP_APP_KEY;

	const [ recipes, setRecipes ] = useState([]);
	const [ search, setSearch ] = useState('');
	const [ query, setQuery ] = useState('chicken');

	useEffect(
		() => {
			function getFetchUrl() {
				//variaveis de ambiente
				const APP_ID = process.env.REACT_APP_APP_ID;
				const APP_KEY = process.env.REACT_APP_APP_KEY;
				return `https://api.edamam.com/search?q=${query}&app_id=${APP_ID}&app_key=${APP_KEY}`;
			}
			async function getRecipes() {
				const result = await fetch(getFetchUrl());
				const data = await result.json();
				setRecipes(data.hits);
			}
			getRecipes();
		},
		[ query ]
	);

	function handleUpdateSearch(e) {
		setSearch(e.target.value);
	}

	function getSearch(event) {
		event.preventDefault();
		setQuery(search);
		setSearch('');
	}

	return (
		<div className="App">
			<form className="search-form" onSubmit={getSearch}>
				<input className="search-bar" type="text" value={search} onChange={handleUpdateSearch} />
				<button className="search-button" type="submit">
					Search
				</button>
			</form>
			<div className="recipes">
				{recipes.map((recipe, index) => (
					<Recipe
						key={index}
						title={recipe.recipe.label}
						calories={recipe.recipe.calories}
						image={recipe.recipe.image}
						ingredients={recipe.recipe.ingredients}
					/>
				))}
			</div>
		</div>
	);
}

export default App;
