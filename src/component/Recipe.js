import React from 'react';
import style from '../component/recipe.module.css';

function Recipe({ title, calories, image, ingredients }) {
	return (
		<div className={style.recipe}>
			<h1>{title}</h1>
			<ol>{ingredients.map((ingredient, index) => <li key={index}>{ingredient.text}</li>)}</ol>
			<p>Calories: {calories}</p>
			<img className={style.image} src={image} alt="Prato" />
		</div>
	);
}

export default Recipe;
